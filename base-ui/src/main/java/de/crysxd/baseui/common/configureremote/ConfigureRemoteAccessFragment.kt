package de.crysxd.baseui.common.configureremote

import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.Animatable2
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.adapter.FragmentViewHolder
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.databinding.ConfigureRemoteAccessFragmentBinding
import de.crysxd.baseui.di.injectViewModel
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.utils.CollapsibleToolbarTabsHelper
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.open
import de.crysxd.octoapp.base.ext.toHtml
import de.crysxd.octoapp.octoprint.isOctoEverywhereUrl
import de.crysxd.octoapp.octoprint.isSpaghettiDetectiveUrl
import de.crysxd.octoapp.octoprint.models.settings.Settings

class ConfigureRemoteAccessFragment : BaseFragment(), InsetAwareScreen {

    override val viewModel by injectViewModel<ConfigureRemoteAccessViewModel>()
    private lateinit var binding: ConfigureRemoteAccessFragmentBinding
    private val helper = CollapsibleToolbarTabsHelper()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        ConfigureRemoteAccessFragmentBinding.inflate(inflater, container, false).also { binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        OctoAnalytics.logEvent(OctoAnalytics.Event.RemoteConfigScreenOpened)
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.header.postDelayed({
                (binding.header.drawable as? Animatable2)?.start()
            }, 1000)
        }

        val adapter = PagerAdapter(requireContext(), childFragmentManager, lifecycle)
        installTabs(adapter)

        binding.description.text = getString(R.string.configure_remote_acces___description).toHtml()
        binding.description.movementMethod = LinkClickMovementMethod(LinkClickMovementMethod.OpenWithIntentLinkClickedListener(requireOctoActivity()))

        viewModel.viewEvents.observe(viewLifecycleOwner) {
            if (it.consumed) {
                return@observe
            }
            it.consumed = true

            when (it) {
                is ConfigureRemoteAccessViewModel.ViewEvent.ShowError -> requireOctoActivity().showDialog(
                    message = it.message,
                    neutralButton = getString(R.string.configure_remote_acces___ignore_issue).takeIf { _ -> it.ignoreAction != null }
                        ?: getString(R.string.show_details),
                    neutralAction = { _ -> it.ignoreAction?.invoke() ?: requireOctoActivity().showDialog(it.exception) },
                )

                is ConfigureRemoteAccessViewModel.ViewEvent.Success -> {
                    requireOctoActivity().showSnackbar(
                        OctoActivity.Message.SnackbarMessage(
                            text = { it.getString(R.string.configure_remote_acces___remote_access_configured) },
                            type = OctoActivity.Message.SnackbarMessage.Type.Positive
                        )
                    )
                }

                is ConfigureRemoteAccessViewModel.ViewEvent.OpenUrl ->
                    Uri.parse(it.url).open(requireOctoActivity())
            }
        }
    }

    private fun installTabs(adapter: PagerAdapter) {
        adapter.sort()
        binding.viewPager.adapter = adapter
        binding.viewPager.offscreenPageLimit = 1
        binding.viewPager.clipChildren = false

        val nextItemVisiblePx = resources.getDimension(R.dimen.margin_1)
        val currentItemHorizontalMarginPx = resources.getDimension(R.dimen.margin_2)
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = ViewPager2.PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position
        }
        binding.viewPager.setPageTransformer(pageTransformer)

        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = adapter.getTitle(position)
        }.attach()

        helper.install(
            octoActivity = requireOctoActivity(),
            tabs = binding.tabs,
            tabsContainer = binding.tabsContainer,
            appBar = binding.appBar,
            toolbar = binding.toolbar,
            toolbarContainer = binding.toolbarContainer,
            showOctoInToolbar = false
        )
    }

    override fun onResume() {
        super.onResume()
        helper.handleResume()
    }

    override fun handleInsets(insets: Rect) {
        helper.handleInsets(insets)
        binding.root.updatePadding(bottom = insets.bottom)
    }

    private class PagerAdapter(val context: Context, fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {
        private var tabs = listOf(
            OctoEverywhereTab,
            SpaghettiDetectiveTab,
            ManualTab
        )

        fun sort() {
            // Sort tabs by importance:
            // - Connected one first
            // - Installed one second
            // - Manual always last if not connected
            // - Tabs with similar importance are shuffled
            val info = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot() ?: return
            tabs = tabs.groupBy { it.getImportance(info) }.map { it.key to it.value.shuffled() }.let {
                it.sortedByDescending { it.first }.map { it.second }.flatten()
            }

            notifyItemRangeChanged(0, tabs.size)
        }

        override fun onBindViewHolder(
            holder: FragmentViewHolder,
            position: Int,
            payloads: MutableList<Any>
        ) {
            (holder.itemView as ViewGroup).clipChildren = false
            (holder.itemView as ViewGroup).clipToPadding = false
            (holder.itemView as ViewGroup).clipToOutline = false
            super.onBindViewHolder(holder, position, payloads)
        }

        override fun getItemCount() = tabs.size
        override fun createFragment(position: Int) = tabs[position].createFragment()
        fun getTitle(position: Int) = context.getString(tabs[position].label)
    }

    abstract class Tab(val label: Int) {
        abstract val importanceNonce: Int
        abstract fun createFragment(): Fragment
        abstract fun isInstalled(info: OctoPrintInstanceInformationV3): Boolean
        abstract fun isConnected(info: OctoPrintInstanceInformationV3): Boolean
        fun getImportance(info: OctoPrintInstanceInformationV3) = listOf(
            if (isInstalled(info)) 2_000 else 0,
            if (isConnected(info)) 10_000 else 0,
        ).sum() + importanceNonce
    }

    object OctoEverywhereTab : Tab(R.string.configure_remote_acces___octoeverywhere___title) {
        override val importanceNonce = (0..1_000).random()
        override fun createFragment() = ConfigureRemoteAccessOctoEverywhereFragment()
        override fun isInstalled(info: OctoPrintInstanceInformationV3) = info.settings?.plugins?.any { it.value is Settings.OctoEverywhere } == true
        override fun isConnected(info: OctoPrintInstanceInformationV3) = info.alternativeWebUrl?.isOctoEverywhereUrl() == true
    }

    object SpaghettiDetectiveTab : Tab(R.string.configure_remote_acces___spaghetti_detective___title) {
        override val importanceNonce = (0..1_000).random()
        override fun createFragment() = ConfigureRemoteAccessSpaghettiDetectiveFragment()
        override fun isInstalled(info: OctoPrintInstanceInformationV3) = info.settings?.plugins?.any { it.value is Settings.SpaghettiDetective } == true
        override fun isConnected(info: OctoPrintInstanceInformationV3) = info.alternativeWebUrl?.isSpaghettiDetectiveUrl() == true
    }

    object ManualTab : Tab(R.string.configure_remote_acces___manual___title) {
        override val importanceNonce = 0
        override fun createFragment() = ConfigureRemoteAccessManualFragment()
        override fun isInstalled(info: OctoPrintInstanceInformationV3) = false
        override fun isConnected(info: OctoPrintInstanceInformationV3) = info.alternativeWebUrl != null &&
                !SpaghettiDetectiveTab.isConnected(info) &&
                !OctoEverywhereTab.isConnected(info)
    }
}