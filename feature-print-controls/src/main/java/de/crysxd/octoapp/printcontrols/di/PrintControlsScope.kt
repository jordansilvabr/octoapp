package de.crysxd.octoapp.printcontrols.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PrintControlsScope