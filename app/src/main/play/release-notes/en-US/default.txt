🕵️  Remote access via The Spaghetti Detective
🔎  Better camera zoom
📸  Share snapshots from the camera feed
🗄  File management: Upload, copy, rename, download...
📚  Plugin library to see which are supported
🔥  As always, many improvements throughout the app

Enjoy the app & happy printing! 🥳
