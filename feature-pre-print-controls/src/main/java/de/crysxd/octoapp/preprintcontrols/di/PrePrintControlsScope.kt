package de.crysxd.octoapp.preprintcontrols.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PrePrintControlsScope