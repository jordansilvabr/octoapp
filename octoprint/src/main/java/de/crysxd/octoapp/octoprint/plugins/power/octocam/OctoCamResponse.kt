package de.crysxd.octoapp.octoprint.plugins.power.octocam

data class OctoCamResponse(
    val torchOn: Boolean?
)