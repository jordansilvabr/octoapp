package de.crysxd.octoapp.octoprint.exceptions

interface RemoteServiceConnectionBrokenException {
    val remoteServiceName: String
}
